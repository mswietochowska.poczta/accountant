import sys

call = sys.argv[1:]
balance = 1000
warehouse = {}
logs = []

actions = ["saldo", "sprzedaz", "zakup", "magazyn", 'przeglad', 'stop']

if call[0] == "saldo":
    print("Wybrałeś akcję saldo.")
    change_in_account = int(call[1])
    description = call[2]
    balance += change_in_account
    logs.append([call[0], change_in_account, description])

    while True:
        print(f"Dostępne akcje: {actions}")
        action = (input("Wprowadź nazwę akcji:"))
        if action == "stop":
            break

        elif action == "saldo":
            print("Wybrałeś akcję saldo.")
            change_in_account = int(input("Podaj wartość zmiany na koncie."))
            description = input("Podaj wartość opisu transakcji")
            balance += change_in_account
            logs.append([action, change_in_account, description])

        elif action == "zakup":
            print("Wybrałeś tryb zakupu.")
            purchase_product_ID = input('Podaj id produktu')
            purchase_until_price = int(input("Podaj cenę zakupu"))
            purchase_nr_items = int(input("Podaj liczbę zakupionych."))

            if purchase_until_price * purchase_nr_items > balance:
                print("Brak wystarczających środków na koncie.")
            if purchase_product_ID in warehouse:

                warehouse[purchase_product_ID] += purchase_nr_items
            else:
                warehouse[purchase_product_ID] = purchase_nr_items
            balance -= purchase_until_price * purchase_nr_items
            action_descripton = f"Zakup {purchase_nr_items} sztuk przedmiotu {purchase_product_ID} " \
                                f"za {purchase_until_price}"
            logs.append([action, warehouse, balance, action_descripton])

        elif action == "sprzedaz":
            print("Wybrałeś tryb sprzedaż.")
            sale_product_ID = input('Podaj id produktu')
            sale_unit_price = int(input("Podaj cenę sprzedazy"))
            sale_nr_items = int(input("Podaj liczbę sprzedanych."))
            if sale_product_ID in warehouse:
                warehouse[sale_product_ID] -= sale_nr_items
            else:
                print('Brak towaru w magazynie')
            balance += sale_unit_price * sale_nr_items
            logs.append([action, warehouse, balance])

        elif action == "konto":
            print("Wybrałeś tryb konto.")
            print(f"Stan konta wynosi: {balance}")
            logs.append([action, warehouse, balance])

        elif action == "magazyn":
            print("Wybrales tryb magazyn.\n")
            id_product = input('Podaj id produktu')
            print(f"Produkt o id {id_product} jest dostepny w ilości {warehouse[id_product]} sztuk.")
            logs.append([action, f'Wysiwetlenie stanu produktu {id_product}'])

        elif action == "przeglad":
            print("Wybrano tryb przegladu")
            print(logs)

elif call[0] == "zakup":
    print("Wybrałeś tryb zakupu.")
    purchase_product_ID = call[1]
    purchase_until_price = int(call[2])
    purchase_nr_items = int(call[3])
    if purchase_until_price * purchase_nr_items > balance:
        print("Brak wystarczających środków na koncie.")

    if purchase_product_ID in warehouse:
        warehouse[purchase_product_ID] += purchase_nr_items
    else:
        warehouse[purchase_product_ID] = purchase_nr_items
    balance -= purchase_until_price * purchase_nr_items
    action_descripton = f"Zakup {purchase_nr_items} sztuk przedmiotu {purchase_product_ID} " \
                        f"za {purchase_until_price}"
    logs.append([call[0], warehouse, balance, action_descripton])

    while True:
        print(f"Dostępne tryby: {actions}")
        action = (input("Wprowadź rodzaj trybu:"))
        if action == "stop":
            break

        elif action == "saldo":
            print("Wybrałeś tryb saldo.")
            change_in_account = int(input("Podaj wartość zmiany na koncie"))
            description = input("Podaj wartość opisu transakcji")
            balance += change_in_account
            logs.append([action, change_in_account, description])

        elif action == "zakup":
            print("Wybrałeś tryb zakupu.")
            purchase_product_ID = input('Podaj id produktu')
            purchase_until_price = int(input("Podaj cenę zakupu"))
            purchase_nr_items = int(input("Podaj liczbę zakupionych."))

            if purchase_until_price * purchase_nr_items > balance:
                print("Brak wystarczających środków na koncie.")
            if purchase_product_ID in warehouse:

                warehouse[purchase_product_ID] += purchase_nr_items
            else:
                warehouse[purchase_product_ID] = purchase_nr_items
            balance -= purchase_until_price * purchase_nr_items
            logs.append([action, warehouse, balance])

        elif action == "sprzedaż":
            print("Wybrałeś tryb sprzedaż.")
            sale_product_ID = input('Podaj id produktu')
            sale_unit_price = int(input("Podaj cenę sprzedazy"))
            sale_nr_items = int(input("Podaj liczbę sprzedanych."))
            if sale_product_ID in warehouse:
                warehouse[sale_product_ID] -= sale_nr_items
            else:
                print('Brak towaru w magazynie')
            balance += sale_unit_price * sale_nr_items
            logs.append([action, warehouse, balance])

        elif action == "konto":
            print("Wybrales tryb konto.")
            print(f"Stan konta wynosi: {balance}")
            logs.append([action, warehouse, balance])

        elif action == "magazyn":
            print("Wybrałeś tryb magazyn.\n")
            id_product = input('Podaj id produktu')
            print(f"Produkt o id {id_product} jest dostępny w ilości {warehouse[id_product]} sztuk.")
            logs.append([action, f'Wysiwetlenie stanu produktu {id_product}'])

        elif action == "przeglad":
            print("Wybrano tryb przegladu.")
            print(logs)


elif call[0] == "sprzedaz":
    print("Wybrałeś tryb sprzedaż.")
    sale_product_ID = call[1]
    sale_unit_price = int(call[2])
    sale_nr_items = int(call[3])
    if sale_product_ID in warehouse:
        warehouse[sale_product_ID] -= sale_nr_items
    else:
        print('Brak towaru w magazynie')
    balance += sale_unit_price * sale_nr_items
    logs.append([call[0], warehouse, balance])

    while True:
        print(f"Dostępne tryby: {actions}")
        action = (input("Wprowadź rodzaj trybu:"))
        if action == "stop":
            break

        elif action == "saldo":
            print("Wybrałeś tryb saldo.")
            change_in_account = int(input("Podaj wartość zmiany na koncie"))
            description = input("Podaj wartość opisu transakcji")
            balance += change_in_account
            logs.append([action, change_in_account, description])

        elif action == "zakup":
            print("Wybrałeś tryb zakupu.")
            purchase_product_ID = input('Podaj id produktu')
            purchase_until_price = int(input("Podaj cenę zakupu"))
            purchase_nr_items = int(input("Podaj liczbę zakupionych."))

            if purchase_until_price * purchase_nr_items > balance:
                print("Brak wystarczających środków na koncie.")
            if purchase_product_ID in warehouse:

                warehouse[purchase_product_ID] += purchase_nr_items
            else:
                warehouse[purchase_product_ID] = purchase_nr_items
            balance -= purchase_until_price * purchase_nr_items
            logs.append([action, warehouse, balance])

        elif action == "sprzedaz":
            print("Wybrałeś tryb sprzedaż.")
            sale_product_ID = input('Podaj id produktu')
            sale_unit_price = int(input("Podaj cene sprzedazy"))
            sale_nr_items = int(input("Podaj liczbe sprzedanych."))
            if sale_product_ID in warehouse:
                warehouse[sale_product_ID] -= sale_nr_items
            else:
                print('Brak towaru w magazynie')
            balance += sale_unit_price * sale_nr_items
            logs.append([action, warehouse, balance])

        elif action == "konto":
            print("Wybrałeś tryb konto.")
            print(f"Stan konta wynosi: {balance}")
            logs.append([action, warehouse, balance])

        elif action == "magazyn":
            print("Wybrales tryb magazyn.\n")
            id_product = input('Podaj id produktu')
            print(f"Produkt o id {id_product} jest dostepny w ilości {warehouse[id_product]} sztuk.")
            logs.append([action, f'Wysiwetlenie stanu produktu {id_product}'])

        elif action == "przeglad":
            print("Wybrano tryb przegladu")
            print(logs)


elif call[0] == "konto":
    print("Wybrales tryb konto.")
    print(f"Stan konta wynosi: {balance}")
    logs.append([call[0], warehouse, balance])

    while True:
        print(f"Action available: {actions}")
        action = (input("Wprowadź tryb:"))
        if action == "stop":
            break

        elif action == "saldo":
            print("Wybrałeś tryb saldo.")
            change_in_account = int(input("Podaj wartość zmiany na koncie"))
            description = input("Podaj wartość opisu transakcji")
            balance += change_in_account
            logs.append([action, change_in_account, description])

        elif action == "zakup":
            print("Wybrałeś tryb zakupu.")
            purchase_product_ID = input('Podaj id produktu')
            purchase_until_price = int(input("Podaj cenę zakupu"))
            purchase_nr_items = int(input("Podaj liczbę zakupionych."))

            if purchase_until_price * purchase_nr_items > balance:
                print("Brak wystarczających środków na koncie.")
            if purchase_product_ID in warehouse:

                warehouse[purchase_product_ID] += purchase_nr_items
            else:
                warehouse[purchase_product_ID] = purchase_nr_items
            balance -= purchase_until_price * purchase_nr_items
            logs.append([action, warehouse, balance])

        elif action == "sprzedaz":
            print("Wybrałeś tryb sprzedaż.")
            sale_product_ID = input('Podaj id produktu')
            sale_unit_price = int(input("Podaj cenę sprzedazy"))
            sale_nr_items = int(input("Podaj liczbę sprzedanych."))
            if sale_product_ID in warehouse:
                warehouse[sale_product_ID] -= sale_nr_items
            else:
                print('Brak towaru w magazynie')
            balance += sale_unit_price * sale_nr_items
            logs.append([action, warehouse, balance])

        elif action == "konto":
            print("Wybrałeś tryb konto.")
            print(f"Stan konta wynosi: {balance}")
            logs.append([action, warehouse, balance])

        elif action == "magazyn":
            print("Wybrałeś tryb magazyn.\n")
            id_product = input('Podaj id produktu')
            print(f"Produkt o id {id_product} jest dostepny w ilości {warehouse[id_product]} sztuk.")
            logs.append([action, f'Wyśwetlenie stanu produktu {id_product}'])

        elif action == "przeglad":
            print("Wybrano tryb przegladu.")
            print(logs)


elif call[0] == "magazyn":
    id_product = call[1]
    print("Wybrałeś tryb magazyn.")
    print(f"Produkt o id {id_product} jest dostępny w ilości {warehouse[id_product]} sztuk.")
    logs.append([call[0], f'Wyśwetlenie stanu produktu {id_product}'])

    while True:
        print(f"Dostępne tryby: {actions}".format)
        action = (input("Wprowadź rodzaj trybu:"))
        if action == "stop":
            break

        elif action == "saldo":
            print("Wybrałeś tryb saldo.")
            change_in_account = int(input("Podaj wartość zmiany na koncie"))
            description = input("Podaj wartość opisu transakcji")
            balance += change_in_account
            logs.append([action, change_in_account, description])

        elif action == "zakup":
            print("Wybrales tryb zakupu.")
            purchase_product_ID = input('Podaj id produktu')
            purchase_until_price = int(input("Podaj cene zakupu"))
            purchase_nr_items = int(input("Podaj liczbe zakupionych."))

            if purchase_until_price * purchase_nr_items > balance:
                print("Brak wystarczających środków na koncie.")
            if purchase_product_ID in warehouse:

                warehouse[purchase_product_ID] += purchase_nr_items
            else:
                warehouse[purchase_product_ID] = purchase_nr_items
            balance -= purchase_until_price * purchase_nr_items
            logs.append([action, warehouse, balance])

        elif action == "sprzedaz":
            print("Wybrałeś tryb sprzedaży.")
            sale_product_ID = input('Podaj id produktu')
            sale_unit_price = int(input("Podaj cene sprzedazy"))
            sale_nr_items = int(input("Podaj liczbe sprzedanych."))
            if sale_product_ID in warehouse:
                warehouse[sale_product_ID] -= sale_nr_items
            else:
                print('Brak towaru w magazynie')
            balance += sale_unit_price * sale_nr_items
            logs.append([action, warehouse, balance])

        elif action == "konto":
            print("Wybrałeś tryb konto.")
            print(f"Stan konta wynosi: {balance}")
            logs.append([action, warehouse, balance])

        elif action == "magazyn":
            print("Wybrales tryb magazyn.\n")
            id_product = input('Podaj id produktu')
            print(f"Produkt o id {id_product} jest dostepny w ilości {warehouse[id_product]} sztuk.")
            logs.append([action, f'Wysiwetlenie stanu produktu {id_product}'])

        elif action == "przeglad":
            print("Wybrano tryb przegladu")
            print(logs)


elif call[0] == "przeglad":
    print("Wybrano tryb przegladu")
    print(logs)
    while True:
        print(f"Dostepne tryby: {actions}")
        action = (input("Wprowadź tryb:"))
        if action == "stop":
            break

        elif action == "saldo":
            print("Wybrałeś tryb saldo.")
            change_in_account = int(input("Podaj wartosc zmiany na koncie"))
            description = input("Podaj wartosc opisu transakcji")
            balance += change_in_account
            logs.append([action, change_in_account, description])

        elif action == "zakup":
            print("Wybrałeś tryb zakupu.")
            purchase_product_ID = input('Podaj id produktu')
            purchase_until_price = int(input("Podaj cene zakupu"))
            purchase_nr_items = int(input("Podaj liczbe zakupionych."))

            if purchase_until_price * purchase_nr_items > balance:
                print("Brak wystarczających środków na koncie.")
            if purchase_product_ID in warehouse:

                warehouse[purchase_product_ID] += purchase_nr_items
            else:
                warehouse[purchase_product_ID] = purchase_nr_items
            balance -= purchase_until_price * purchase_nr_items
            logs.append([action, warehouse, balance])

        elif action == "sprzedaz":
            print("Wybrałeś tryb sprzedaż.")
            sale_product_ID = input('Podaj id produktu')
            sale_unit_price = int(input("Podaj cene sprzedazy"))
            sale_nr_items = int(input("Podaj liczbe sprzedanych."))
            if sale_product_ID in warehouse:
                warehouse[sale_product_ID] -= sale_nr_items
            else:
                print('Brak towaru w magazynie')
            balance += sale_unit_price * sale_nr_items
            logs.append([action, warehouse, balance])

        elif action == "konto":
            print("Wybrales tryb konto.")
            print(f"Stan konta wynosi: {balance}")
            logs.append([action, warehouse, balance])

        elif action == "magazyn":
            print("Wybrales tryb magazyn.\n")
            id_product = input('Podaj id produktu')
            print(f"Produkt o id {id_product} jest dostepny w ilości {warehouse[id_product]} sztuk.")
            logs.append([action, f'Wyświetlenie stanu produktu {id_product}'])

        elif action == "przeglad":
            print("Wybrano tryb przegladu")
            print(logs)
